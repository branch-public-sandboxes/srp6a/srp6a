const assert = require('assert');
const srp = require("../lib/srp");

const params = srp.params[4096]
const login = "alice"
const password = "password123"


describe('SRP', function () {
    it('Create Verifier', function () {
        const salt = BigInt("12345")
        const verifier = srp.computeVerifier(params, salt, identity, password);
        assert.equal(verifier, BigInt("325054598261923097429489977408774426429250477942275047312073146456551445986535095708229557177357011084967290252914850869423262868023304661028175883141140001053548219537097639806341541621288987237985460279676615933409508286172599973752372280709245368592571935763221380363951179321223064815885830190833970492724410363434413760483259889980870685140571002784006182498432695764565197194264498630647473961634285696389112163885024731490764848042196583057715558657751018296677577733452162656166800398417961787657989094952336907427835478838206220157551453764997931359639706275175649944947605913769664602517375430026280942108648887821257641163579368230561070717013134971107413669188205771608077233790935563891696378005242202832079547927866262631841179525782082342210555966180892933883428350537609351300243871426365794716110206957890648145049696310766726398752754257956765493751711886415678258421540302717153508217119065884294554173393291445952405890530884245618143000544030877080164718186275411247580851590084135934440781840968786792232634946022022880285704569717271042894907320132828846798254282803217178516945378638035079796566802799796312844297204960097243995751721940083728780700167498198612864533215731223537206310165547372992666286352672"));
    });

    it('Test Client', async function () {
        await signUp()
        await step1()

        async function signUp() {
            const salt = srp.getRandomSalt()
            const verifier = srp.computeVerifier(params, salt, "alice", "password123");
            await fetch('http://localhost:8080/srp/signup', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    login,
                    salt,
                    verifier
                })
            })
        }

        async function step1() {
            const response = (await fetch('http://localhost:8080/srp/step1', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    login,
                })
            }))
            const json = await response.json()
            const salt = BigInt(json.salt)
            const B = BigInt(json.B)

            let srpClient = srp.Client(params, salt, "alice", "password123")
            srpClient.setB(B)
            const A = srpClient.computeA()
            const M1 = srpClient.computeM1()

            const responseAM = (await fetch('http://localhost:8080/srp/step2', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    login,
                    A,
                    M1
                })
            }))
            const jsonAM = await responseAM.json()
            console.log('Data: ', jsonAM); // выводим ответ в консоль
            srpClient.checkM2(BigInt(jsonAM.M2))
        }
    });
});