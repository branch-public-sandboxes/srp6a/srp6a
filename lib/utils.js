const { Buffer } = require('buffer')

BigInt.prototype["toJSON"] = function () {
    return this.toString();
};

const bigIntToBuffer = (bigint) => {
    const numBytes = Math.ceil(bigint.toString(16).length / 2);
    const byteArray = new Uint8Array(numBytes);
    for (let i = 0; i < numBytes; i++) {
        byteArray[i] = Number(bigint & BigInt(255));
        bigint >>= BigInt(8);
    }
    return Buffer.from(byteArray.reverse());
}

const bufferToBigInt = (buffer) => {
    const byteArray = new Uint8Array(buffer.reverse());
    let bigInt = BigInt(0);
    for (let i = byteArray.length - 1; i >= 0; i--) {
        bigInt <<= BigInt(8);
        bigInt |= BigInt(byteArray[i]);
    }
    return bigInt
}

const bitLength = (bigint) => {
    return bigint.toString(2).length
}

module.exports = {
    bigIntToBuffer,
    bufferToBigInt,
    bitLength
};
