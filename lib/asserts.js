const { Buffer } = require('buffer')
const assert = require("assert");

const assertIsBuffer = (arg, argname) => {
    argname = argname || "arg";
    assert_(Buffer.isBuffer(arg), "Type error: " + argname + " must be a buffer");
}

function assertIsNBuffer(arg, params, argname) {
    argname = argname || "arg";
    assert_(Buffer.isBuffer(arg), "Type error: " + argname + " must be a buffer");
    if (arg.length != params.N_length_bits / 8)
        assert_(false, argname + " was " + arg.length + ", expected " + (params.N_length_bits / 8));
}

const assert_ = (val, msg) => {
    if (!val)
        throw new Error(msg || "assertion");
}

const assertIsBignum = (arg) => {
    assert.equal(arg.constructor.name, "BigInt");
}

module.exports = {
    assertIsBuffer,
    assertIsNBuffer,
    assert_,
    assertIsBignum
}