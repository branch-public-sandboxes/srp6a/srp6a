const { Buffer } = require('buffer')
const crypto = require('crypto-browserify');
const {assert_, assertIsBignum, assertIsBuffer, assertIsNBuffer} = require("./asserts");
const assert = require("assert");

const bigintCryptoUtils = require('bigint-crypto-utils');
const {bigIntToBuffer, bufferToBigInt, bitLength} = require("./utils");

const zero = BigInt(0);

module.exports = {
    params: require('./params'),
    getRandomSalt,
    computeVerifier,
    Client
};

function getRandomSalt() {
    // const bytes = new Uint8Array(32);
    const bytes = crypto.randomBytes(32);
    const bytesHex = bytes.reduce((o, v) => o + ('00' + v.toString(16)).slice(-2), '');
    return BigInt('0x' + bytesHex);
}

function computeVerifier(params, salt, login_I, password_P) {
    salt = bigIntToBuffer(salt)
    login_I = Buffer.from(login_I)
    password_P = Buffer.from(password_P)

    assertIsBuffer(salt, "salt (salt)");
    assertIsBuffer(login_I, "identity (I)");
    assertIsBuffer(password_P, "password (P)");

    let x = getX(params, salt, login_I, password_P)
    const v_num = bigintCryptoUtils.modPow(params.g, bufferToBigInt(x), params.N)
    const verifier = padToN(v_num, params);
    return bufferToBigInt(verifier);
}

function getX(params, salt, login_I, password_P) {
    assertIsBuffer(salt, "salt (salt)");
    assertIsBuffer(login_I, "login (I)");
    assertIsBuffer(password_P, "password (P)");

    const hashIP = crypto.createHash(params.hash)
        .update(Buffer.concat([login_I, Buffer.from(':'), password_P]))
        .digest();
    return crypto.createHash(params.hash)
        .update(salt)
        .update(hashIP)
        .digest();
}

function padToN(number, params) {
    assertIsBignum(number);
    return padTo(bigIntToBuffer(number), params.N_length_bits / 8);
}

function padTo(n, len) {
    assertIsBuffer(n, "n");
    const padding = len - n.length;
    assert_(padding > -1, "Negative padding. Very uncomfortable.");

    const result = Buffer.alloc(len);
    result.fill(0, 0, padding);
    n.copy(result, padding);
    assert.equal(result.length, len);
    return result;
}

function getA(params, a_num) {
    assertIsBignum(a_num);
    if (Math.ceil(bitLength(a_num) / 8) < 256 / 8) {
        console.warn("getA: client key length", bitLength(a_num), "is less than the recommended 256");
    }
    return padToN(bigintCryptoUtils.modPow(params.g, a_num, params.N), params);
}

function getk(params) {
    const k_buf = crypto
        .createHash(params.hash)
        .update(padToN(params.N, params))
        .update(padToN(params.g, params))
        .digest();
    return bufferToBigInt(k_buf);
}

function getM1(params, A_buf, B_buf, S_buf) {
    assertIsNBuffer(A_buf, params, "A");
    assertIsNBuffer(B_buf, params, "B");
    assertIsNBuffer(S_buf, params, "S");
    return crypto.createHash(params.hash)
        .update(A_buf).update(B_buf).update(S_buf)
        .digest();
}

function getM2(params, A_buf, M_buf, K_buf) {
    assertIsNBuffer(A_buf, params, "A");
    assertIsBuffer(M_buf, "M");
    assertIsBuffer(K_buf, "K");
    return crypto.createHash(params.hash)
        .update(A_buf).update(M_buf).update(K_buf)
        .digest();
}

function getu(params, A, B) {
    assertIsNBuffer(A, params, "A");
    assertIsNBuffer(B, params, "B");
    const u_buf = crypto.createHash(params.hash)
        .update(A)
        .update(B)
        .digest();
    return bufferToBigInt(u_buf);
}

function client_getS(params, k_num, x_num, a_num, B_num, u_num) {
    assertIsBignum(k_num);
    assertIsBignum(x_num);
    assertIsBignum(a_num);
    assertIsBignum(B_num);
    assertIsBignum(u_num);
    const g = params.g;
    const N = params.N;
    if (zero >= B_num || N <= B_num)
        throw new Error("invalid server-supplied 'B', must be 1..N-1");

    const exp = (u_num * x_num) + a_num
    const tmp = bigintCryptoUtils.modPow(g, x_num, N) * k_num
    const S_num = bigintCryptoUtils.modPow(B_num - tmp, exp, N);

    return padToN(S_num, params);
}

function equal(buf1, buf2) {
    let mismatch = buf1.length - buf2.length;
    if (mismatch) {
        return false;
    }
    for (let i = 0; i < buf1.length; i++) {
        mismatch |= buf1[i] ^ buf2[i];
    }
    return mismatch === 0;
}

function Client(params, salt, login, password) {
    if (!(this instanceof Client)) {
        return new Client(params, salt, login, password);
    }

    let salt_buf = bigIntToBuffer(salt)
    let identity_buf = Buffer.from(login)
    let password_buf = Buffer.from(password)

    assertIsBuffer(salt_buf, "salt (salt)");
    assertIsBuffer(identity_buf, "identity (I)");
    assertIsBuffer(password_buf, "password (P)");
    this._private = {
        params: params,
        k_num: getk(params),
        x_num: bufferToBigInt(getX(params, salt_buf, identity_buf, password_buf)),
        a_num: getRandomSalt() // Подойдёт любое рандомное число
    };
    this._private.A_buf = getA(params, this._private.a_num);
}

Client.prototype = {
    computeA: function computeA() {
        return bufferToBigInt(this._private.A_buf);
    },
    setB: function setB(B_num) {
        const p = this._private;
        const u_num = getu(p.params, p.A_buf, bigIntToBuffer(B_num));
        const S_buf = client_getS(p.params, p.k_num, p.x_num, p.a_num, B_num, u_num);
        p.M1_buf = getM1(p.params, p.A_buf, bigIntToBuffer(B_num), S_buf);
        p.M2_buf = getM2(p.params, p.A_buf, p.M1_buf, S_buf);
    },
    computeM1: function computeM1() {
        if (this._private.M1_buf === undefined)
            throw new Error("incomplete protocol");
        return bufferToBigInt(this._private.M1_buf);
    },
    checkM2: function checkM2(serverM2_buf) {
        if (!equal(this._private.M2_buf, bigIntToBuffer(serverM2_buf)))
            throw new Error("server is not authentic");
    }
};